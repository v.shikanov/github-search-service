import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import List from "./GitHubUsers/List";
import Profile from "./GitHubUsers/Profile";

export default class App extends Component {
    render() {
        return (
            <Router>

            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{paddngBottom: 30}}>
                    <Link className="nav-link" to="/github-users/list">Home</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"> </span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/github-users/list">GitHub Users List</Link>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div className="container">
                    <Route path="/github-users/list" component={List} />
                    <Route path="/github-users/profile/:id" component={Profile} />
                </div>
            </div>
            </Router>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
