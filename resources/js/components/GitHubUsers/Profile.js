import React, {Component} from 'react';
import axios from 'axios';

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: null,
                username: null,
                avatar_url: null
            }
        };

        this.toggleDisplayUser = this.toggleDisplayUser.bind(this);
    }

    componentDidMount() {
        axios.get('/api/v1/github-users/' + this.props.match.params.id)
                .then((response) => {
                    if (response.data.id !== undefined) {
                        console.log(response);

                        this.setState({user: response.data});
                    }
                });
    }

    render() {
        return (
            <div>
                <h4>Profile</h4>

                {this.state.user.id &&
                <form>
                    <div className="form-group">
                        <label>ID</label>
                        <div>{this.state.user.id}</div>
                    </div>

                    <div className="form-group">
                        <label>Username</label>
                        <div><a href={`https://github.com/${this.state.user.username}`}>{this.state.user.username}</a></div>
                    </div>

                    <div className="form-group">
                        <label>Avatar</label>
                        <div><img src={this.state.user.avatar_url} width="100" height="100" /></div>
                    </div>

                    <button type="submit" className="btn btn-primary" onClick={this.toggleDisplayUser}>
                        {this.state.user.is_hide ? 'Show' : 'Hide'}
                    </button>
                </form>
                }
            </div>
        );
    }

    toggleDisplayUser(e) {
        e.preventDefault();
        axios.put('/api/v1/github-users/' + this.props.match.params.id, {is_hide: !this.state.user.is_hide})
            .then((response) => {
                this.setState({user: response.data})
            });
    }
}
