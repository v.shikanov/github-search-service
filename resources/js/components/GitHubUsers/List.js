import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: false,
        };

        this.searchForGitHubUsers = this.searchForGitHubUsers.bind(this);
    }

    componentDidMount() {
        axios.get('/api/v1/github-users')
            .then((response) => {
                if (response.data.data.length > 0) {
                    this.setState({users: response.data.data});
                }
            });
    }

    render() {
        return (
            <div>
            <h4>Search for github users</h4>
            <input type="text" id="search-term" /> <input type="submit" onClick={this.searchForGitHubUsers} />

                {this.state.users &&
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Avatar</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.users.map((v) =>
                    <tr>
                        <td>{v.id}</td>
                        <td><Link className="nav-link" to={`/github-users/profile/${v.id}`}>{v.username}</Link></td>
                        <td><img src={v.avatar_url} width="100" height="100" /></td>
                    </tr>
                    )}

                    </tbody>
                </table>
                ||
                <p>Type search query</p>
                }
            </div>
        );
    }

    searchForGitHubUsers(e) {
        e.preventDefault();
        axios.post('/api/v1/github-users/search', {term: document.getElementById('search-term').value})
            .then((response) => {
                this.setState({users: response.data.data})
            });
    }
}
