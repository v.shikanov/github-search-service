<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->create([
            'email' => 'foo@bar.com',
            'password' => bcrypt('foobar'),
            'api_token' => 'test',
        ]);
    }
}
