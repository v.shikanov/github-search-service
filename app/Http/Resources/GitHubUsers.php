<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GitHubUsers extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'github_id' => $this->github_id,
            'username' => $this->username,
            'avatar_url' => $this->avatar_url,
            'is_hide' => $this->is_hide,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
