<?php

namespace App\Jobs;

use App\Models\GitHubUser;
use App\Services\GitHubService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class StoreGitHubUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $usersCollection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $collection)
    {
        $this->usersCollection = $collection;
    }

    /**
     * Execute the job.
     *
     * @param \App\Services\GitHubService $gitHubService
     *
     * @return void
     */
    public function handle(GitHubService $gitHubService)
    {
        $gitHubService->updateOrCreateManyUsers(
            $this->usersCollection
        );
    }
}
