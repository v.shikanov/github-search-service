<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GitHubUser
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GitHubUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GitHubUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GitHubUser query()
 * @mixin \Eloquent
 */
class GitHubUser extends Model
{
    use UsesUuid;

    protected $table = 'github_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'github_id',
        'avatar_url',
        'is_hide',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_hide' => 'bool',
    ];

    /**
     * @var array
     */
    protected $guarded = [];
}
