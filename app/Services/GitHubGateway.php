<?php

namespace App\Services;

use App\Jobs\StoreGitHubUsers;
use App\Models\GitHubUser;
use Github\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class GitHubGateway
{
    /**
     * @var \App\Services\GitHubService
     */
    protected $gitHubService;

    /**
     * @param \App\Services\GitHubService $gitHubService
     */
    public function __construct(GitHubService $gitHubService)
    {
        $this->gitHubService = $gitHubService;
    }

    /**
     * @return array|null
     */
    public function search($term): ?Collection
    {
        /**
         * @var \Github\Client $client
         */
        $client = resolve('clients.github');
        $response = $client->search()->users($term);

        if (!isset($response['items'])) {
            return null;
        }

        $usersCollection = collect($response['items']);

        if ($usersCollection->isEmpty()) {
            return null;
        }

        return $this->gitHubService->updateOrCreateManyUsers(
            $this->prepareData($usersCollection)
        );
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    protected function prepareData(Collection $collection): Collection
    {
        return $collection->map(function ($user) {
            return [
                'username' => $user['login'],
                'github_id' => $user['id'],
                'avatar_url' => $user['avatar_url'],
            ];
        });
    }
}
